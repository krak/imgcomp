unit uimg;

{$MODE DELPHI}
{$codepage UTF8}

interface

uses fpimage, v8napi, base64;

type
  TFPGraphics = class(TV8UserObject)
  public
    function Resize(RetValue: PV8Variant; Params: PV8ParamArray; const ParamCount: integer): boolean;
  end;

implementation
uses classes, fpwritepng, fpwritejpeg, fpimgcanv, //sysutils, 
  fpreadgif, fpreadjpeg, fpreadpcx, fpreadpng, fpreadpsd, fpreadtga, fpreadtiff, fpreadxpm, fpreadxwd, fpreadbmp;

{  TGraphics  }
function TFPGraphics.Resize(RetValue: PV8Variant; Params: PV8ParamArray; const ParamCount: integer): boolean;
var 
  strstream : TStringStream;
  ext : AnsiString;
  strPic : AnsiString;
  Img : TFPMemoryImage;
  Dst : TFPMemoryImage;
  cw : TFPCustomImageWriter;
  cr : TFPCustomImageReader;
  ic : TFPImageCanvas;
  min : Integer;
  minSize : Integer;
  ratio : double;
  // logfile : TextFile;
  W, H : LongInt;
begin
  ext := V8AsAString(@Params[2]);
  minSize := V8AsInt(@Params[3]);
  if minSize <= 0 then minSize := 500;
  // AssignFile(logfile, 'D:\Apps\VCS\delphi_repo\1c\imgcomp\imgcomp.log');
  // Rewrite(logfile);
  // WriteLn(logfile, 'Param 2: ' + ext);
  // WriteLn(logfile, 'Param 3: ' + IntToStr(minSize));
  // Flush(logfile);
  try

    try
      if (ext = 'png') then cw := TFPWriterPNG.Create
      else cw := TFPWriterJPEG.Create;

      if (ext = 'gif') then cr := TFPReaderGIF.Create
      else if (ext = 'png') then cr := TFPReaderPNG.Create
      else if (ext = 'pcx') then cr := TFPReaderPCX.Create
      else if (ext = 'psd') then cr := TFPReaderPSD.Create
      else if (ext = 'tga') then cr := TFPReaderTarga.Create
      else if (ext = 'tif') or (ext = 'tiff') then cr := TFPReaderTIFF.Create
      else if (ext = 'xpm') then cr := TFPReaderXPM.Create
      else if (ext = 'xwd') then cr := TFPReaderXWD.Create
      else if (ext = 'jpg') or (ext = 'jpeg') then cr := TFPReaderJPEG.Create
      else cr := TFPReaderBMP.Create;

      strPic := DecodeStringBase64(V8AsAString(@Params[1]));

      try
        Img := TFPMemoryImage.Create(0,0);
        // WriteLn(logfile, 'Создали пустую картинку');
        // Flush(logfile);
        try
          strstream := TStringStream.Create(strPic);
          // WriteLn(logfile, 'Загрузили строку в поток');
          // Flush(logfile);
          strstream.Position := 0;
          try
            Img.LoadFromStream(strstream, cr);
            // WriteLn(logfile, 'Загрузили исходную картинку');
          except
            // on e: Exception do WriteLn(logfile, E.Message);
          end;
          // Flush(logfile);
        finally
          strstream.Free;
        end;

        W := Img.Width;
        H := Img.Height;
        if W < H then min := W else min := H;
        if min > minSize then begin
          ratio := minSize / min;
          // WriteLn(logfile, 'Нашли пропорцию: ' + FloatToStr(ratio));
          // Flush(logfile);
          W := round(W * ratio);
          H := round(H * ratio);
        end;

        try
          Dst := TFPMemoryImage.Create(W,H);
          try
            ic := TFPImageCanvas.create(Dst);
            try
              ic.StretchDraw(0, 0, W-1, H-1, Img);
              // WriteLn(logfile, 'Установили размеры (W,H): ' + IntToStr(W) + ' x ' + IntToStr(H));
            except
              // on e: Exception do WriteLn(logfile, E.Message);
            end;
          finally
            ic.Free;
          end;
          // Flush(logfile);
          try
            strstream := TStringStream.Create('');
            Dst.SaveToStream(strstream, cw);
            // WriteLn(logfile, 'Сохраняем измененную картинку в поток');
            // Flush(logfile);
            strPic :=strstream.DataString;
            // WriteLn(logfile, 'Получаем строку из потока');
            // Flush(logfile);
          finally
            strstream.Free;
          end;
        finally
          Dst.Free;
        end;
        // Flush(logfile);
      finally
        Img.Free;
      end;
    finally
      cw.Free;
    end;
    // WriteLn(logfile, 'Закодировали строку: ' + EncodeStringBase64(strPic));
    // Flush(logfile);
  finally
    // CloseFile(logfile);
  end;
  V8SetString(RetValue, EncodeStringBase64(strPic));
  Resize := true;
end;

end.