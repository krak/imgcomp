library imgcomp;

{$MODE DELPHI}
{$codepage UTF8}

{$IFDEF WIN32}
{$LIBSUFFIX '32'}
{$ENDIF}

{$IFDEF WIN64}
{$LIBSUFFIX '64'}
{$ENDIF}

uses
  v8napi, uimg;

exports DestroyObject, GetClassNames, GetClassObject;

begin
  with ClassRegList.RegisterClass(TFPGraphics, 'ImageOperation', 'TFPGraphics') do begin
  	AddFunc('Resize', 'ИзменитьРазмер', @TFPGraphics.Resize, 3);
  end;
end.